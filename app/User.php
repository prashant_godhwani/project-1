<?php

namespace App;

use Carbon\Carbon;
use DateTime;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name','mobile','date_of_birth', 'email', 'location','age',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getDateOfBirthAttribute($value)
    {
        return Carbon::parse($value)->format(' jS  F Y ');
    }

    public function setDateOfBirthAttribute($value)
    {
        $this->attributes['date_of_birth'] =  DateTime::createFromFormat('d/m/Y', $value);
    }
}
