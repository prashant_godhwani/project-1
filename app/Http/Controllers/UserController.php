<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    public function index(){
        return view('listing');
    }

    public function getUserListing(){
        return Datatables::eloquent(User::query())->addColumn('action', function ($user) {
                return '<a href="user-view/'.$user->id.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
            })
                ->editColumn('id', '{{$id}}')->make(true);
    }

    public function viewUser(User $user){
        return view('view',compact('user'));
    }

    public function updateUser(Request $request,User $user)
    {
        $rules = array(
                'first_name' => 'min:3|max:190|required',
                'last_name' => 'min:3|max:190|required',
                'email' => 'unique:users,email,' . $user->id,
                'mobile' => 'digits_between:10,10|required',
                'location' => 'required',
                'age' => 'numeric|required'
            );
        $validator = Validator::make($request->toArray(), $rules);
        if ($validator->fails())
        {
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()

            ), 400); // 400 being the HTTP code for an invalid request.
        }
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->age = $request->age;
            $user->date_of_birth = $request->date_of_birth;
            $user->location = $request->location;
            $user->email = $request->email;
            $user->mobile = $request->mobile;
            $user->save();
        return Response::json(array('success' => true), 200);

    }
}
