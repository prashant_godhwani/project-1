<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'UserController@index')->name('user.listing');

Route::get('/user-data', 'UserController@getUserListing');

Route::get('/user-view/{user?}', 'UserController@viewUser')->name('user.view');

Route::post('/user-update/{user?}', 'UserController@updateUser')->name('user.update');
