
@extends('layouts.dashboard')

@section('content')
    <!-- Start Page content -->
    <div class="content">
        <div class="container-fluid">


            <div class="row">
                <div class="col-md-12">
                    <div class="card-box">
                        <h4 class="m-t-0 m-b-30 header-title">Add Model</h4>
                        @include('layouts.error')
                            <form role="form" id="idForm">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="exampleInputPassword1">First Name</label>
                                <input type="text" class="form-control" name="first_name" value="{{ old('first_name', $user->first_name) }}" placeholder="First Name" />
                                <small id="emailHelp" class="form-text text-muted">Model Name</small>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Last Name</label>
                                <input type="text" class="form-control" name="last_name" value="{{ old('last_name', $user->last_name) }}" placeholder="Last Name" />
                                <small id="emailHelp" class="form-text text-muted">Model Name</small>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Email</label>
                                <input type="email" class="form-control" name="email" value="{{ old('email', $user->email) }}" placeholder="Email" />
                                <small id="emailHelp" class="form-text text-muted">Model Name</small>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Phone</label>
                                <input type="text" class="form-control" name="mobile" value="{{ old('mobile', $user->mobile) }}" placeholder="Mobile" />
                                <small id="emailHelp" class="form-text text-muted">Model Name</small>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Date Of Birth</label>
                                <input type="text" class="form-control datepicker" id="dob" name="date_of_birth" value="{{ old('date_of_birth', \Carbon\Carbon::parse($user->date_of_birth)->format('d/m/Y')) }}" placeholder="Date Of Birth" />
                                <small id="emailHelp" class="form-text text-muted">Model Name</small>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Age</label>
                                <input type="text" class="form-control" id="age" name="age" value="{{ old('age', $user->age) }}" placeholder="Age" />
                                <small id="emailHelp" class="form-text text-muted">Model Name</small>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Location</label>
                                <input type="text" id="autocomplete" class="form-control" name="location" value="{{ old('first_name', $user->location) }}" placeholder="Location" />
                                <small id="emailHelp" class="form-text text-muted">Model Name</small>
                            </div>
                            @if(isset($user->first_name))
                                <button type="submit" class="btn btn-primary">Update User</button>
                             @else
                                <button type="submit" class="btn btn-primary">Add User</button>
                             @endif
                        </form>
                    </div>
                </div>
                <!-- end col -->

            </div>
            <!-- end row -->
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            todayHighlight : true,
        });
    </script>
    <script type="text/javascript">
        $("#idForm").submit(function(e) {

            @isset($user->email)
                var url = "http://127.0.0.1/project1/public/user-update/{{$user->id}}"; // the script where you handle the form input.
            @else
                var url = "http://127.0.0.1/project1/public/user-update";
            @endif

            $.ajax({
                method: "POST",
                url: url,
                data: $("#idForm").serialize(), // serializes the form's elements.
                success: function(data)
                {
                    console.log(data);
                    location.href = "http://127.0.0.1/project1/public/"// show response from the php script.
                },
                error: function(data){
                    var response = JSON.parse(data.responseText);
                    var errorString = '<div class="alert alert-danger"><ul>';
                    $.each( response.errors, function( key, value) {
                        errorString += '<li>' + value + '</li>';
                    });
                    errorString += '</ul></div>';
                    $('#errors').html(errorString);
                }
            });

            e.preventDefault(); // avoid to execute the actual submit of the form.
        });
    </script>
    <script src="https://maps.googleapis.com/maps/api/js??key=AIzaSyCcMoEKA-eDdnMsrzYefS96t7yv8d6Kjuw&v=3.exp&sensor=false&libraries=places"></script>
    <script>
        var autocomplete;
        function initialize() {
            autocomplete = new google.maps.places.Autocomplete(
                /** @type {HTMLInputElement} */(document.getElementById('autocomplete')),
                { types: ['geocode'] });
            google.maps.event.addListener(autocomplete, 'place_changed', function() {
            });
        }
    </script>

@endsection
