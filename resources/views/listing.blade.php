@extends('layouts.dashboard')

@section('content')
    <!-- Start Page content -->
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card-box table-responsive">
                        <h4 class="m-t-0 header-title">User Listing</h4>
                        <p class="text-muted font-14 m-b-30">
                            This is User Listing made using DataTables
                        </p>
                        <div class="mb-3">
                            <div class="row">
                                <div class="col-12 text-sm-center form-inline">
                                    <div class="form-group mr-2">
                                        <a href="{{route('user.view')}}">
                                            <button id="demo-btn-addrow" class="btn btn-primary"><i class="mdi mdi-plus-circle mr-2"></i> Add User</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table id="users-table" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>User ID</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Mobile</th>
                                <th>DOB</th>
                                <th>Age</th>
                                <th>Location</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div> <!-- end row -->
@stop

@section('scripts')
                <script src="{{asset('admin/plugins/datatables/jquery.dataTables.min.js')}}"></script>
                <script src="{{asset('admin/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
                <script src="{{asset('admin/plugins/datatables/dataTables.keyTable.min.js')}}"></script>

                <script type="text/javascript">
                    $(function() {
                        $('#users-table').DataTable({
                            processing: true,
                            serverSide: true,
                            ajax: 'http://127.0.0.1/project1/public/user-data',
                            columns: [
                                {data: 'id'},
                                {data: 'first_name'},
                                {data: 'last_name'},
                                {data: 'email'},
                                {data: 'mobile'},
                                {data: 'date_of_birth'},
                                {data: 'age'},
                                {data: 'location'},
                                {data: 'action', name: 'action', orderable: false, searchable: false}
                            ],
                            pageLength: 5
                        });
                    });
                </script>

    @endsection